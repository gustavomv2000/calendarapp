const express      = require("express");
const manageAccess = require('./manageAccess');
const manageTasks  = require('./manageTasks');
const bodyParser   = require('body-parser');
const app          = express();

server();

function server() {

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(bodyParser.json())

    app.get('/getTasks', manageTasks.displayTasks);

    app.put('/updateStatus', manageTasks.updateStatus);

    app.post('/user', manageAccess.createUser);

    app.post('/authenticate', manageAccess.authenticateUser);

    app.post('/task', manageTasks.createTask);

    app.listen(80);
}

