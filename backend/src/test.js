var crypt = require('./crypt');
var mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'calendar'
});

connection.connect((err) => {
    if (err) throw err;
    console.log('Connected to database!');
});

var sql = `SELECT id, name, password FROM user WHERE username = 'teste3.encrypt@teste.com' LIMIT 1`;
connection.query(sql, (err, result, fields) => {
    result = result[0];
    var s = result.password;
    console.log(crypt.decrypt(s));
})