const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'calendar'
});

module.exports = {
    createTask(req, res) {
        var date = req.query.date;
        var status = req.query.status;
        var description = req.query.description;
        var title = req.query.title;
        var user = req.query.user;

        connection.connect(function(err) {
            if(err) throw err;
            var sql = `INSERT INTO task 
                (dateTask, statusTask, 
                descriptionTask, title, userId)
                VALUES ?`;
            var values = [
                [date, status, description, title, user]
            ];
            connection.query(sql, [values], (err, result, field) => {
                if(err) 
                    res.send('An error occured adding a task!');
                else
                    res.send('Task added correctly!');
            })
        })
    },

    displayTasks(req, res) {
        var user = req.query.user;

        connection.connect(function(err) {
            if(err) throw err;
            var sql = `SELECT * FROM task WHERE userId = '${user}' LIMIT 100`;
            connection.query(sql, (err, result, field) => {
                if(err) throw err;
                res.send(result);
            });
        });
    }, 

    updateStatus(req, res) {
        var id = req.query.id;
        
        connection.connect(function(err) {
            var sql = `UPDATE task SET ? WHERE ?`;
            var values = [
                'Conlcuído', 
                id
            ];
            connection.query(sql, [{ statusTask: 'Concluído' }, { id: id }], (err, result, fields) => {
                if(result.affectedRows == 0)
                    res.send('Error updating task status!');
                else
                    res.send('Status updated!');
            })
        })
    }
}