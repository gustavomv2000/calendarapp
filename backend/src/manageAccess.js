const mysql = require('mysql');
const crypt = require('./crypt');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'calendar'
});

connection.connect((err) => {
    if (err) throw err;
    console.log('Connected to database!');
});

module.exports = {
    async createUser(req, res) {
        var name = req.body.name;
        var username = req.body.username;
        var password = req.body.password;
        console.log('name: ', name);
        password = crypt.encrypt(password);

        var sql = "INSERT INTO user (name, username, password) VALUES ?";  
        var values = [
            [name, username, password]
        ];
        connection.query(sql, [values], function (err, result) {
            if(err)  
                res.send('A problem occured creating user!');
            else 
                res.send('User created!');
        });
    },

    async authenticateUser(req, res) {
        var username = req.body.username;
        var password = req.body.password;
        password = crypt.encrypt(password);

        var sql = `SELECT id, name FROM user WHERE username = '${username}' AND password = '${password}' LIMIT 1`;
        connection.query(sql, (err, result, fields) => {
            // if(err) throw err;
            if(Object.keys(result).length == 0) {
                res.send('Error authenticating user!');
            }
            else if(Object.keys(result).length == 1) {
                res.send('User authenticated!');
            }
            else {
                res.send('Unexpected error ocurred!')
            }
        })
    },

    changePassword(user, password) {
        
    }, 

    forgotPassword(user) {

    },

    changeName(user) {

    }
}