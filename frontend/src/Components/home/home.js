import React from 'react';

import './home.css';
import Header from '../Header/Header'
import Calendar from '../Calendar/Calendar';

export default function Home() {
  return (
    <section>
      <Header />
      <div className="main-home">
         <Calendar />
      </div>
    </section>
  );
}
