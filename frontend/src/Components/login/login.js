import React from 'react';

import './login.css'

export default function Login() {
  return (
    <section className="section-login">
        <div className="form">
            <h1 className="login-title">CalendarApp</h1>
            <div className="email">
                <input 
                    type = "text"
                    placeholder = "e-mail"
                />
            </div>
            <div className="password">
                <input 
                    type = "text"
                    placeholder = "password"
                />
            </div>
            <div className="btn">
                <a href="/" className="btn-link">Login</a>
            </div>
        </div>
    </section>
  );
}

