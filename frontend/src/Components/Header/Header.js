import React from 'react';

import './Header.css';
import User from '../../img/user.png'

export default class Header extends React.Component {
    render() {
        return (
            <section className="section-header">
                <div className="main">
                    <h1 className="header-title">CalendarApp</h1>
                    <div className="user">
                        <img src={User} alt="User"/>
                    </div>
                </div>
            </section>
        )
    }
}