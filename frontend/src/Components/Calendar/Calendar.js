import React from 'react';
import moment from 'moment';

import './Calendar.css';

export default class Calendar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dateObject: moment()
        }
        this.buildCalendar = this.buildCalendar.bind(this);
        this.splitInWeeks = this.splitInWeeks.bind(this);
        this.getYear = this.getYear.bind(this);
        this.getMonth = this.getMonth.bind(this);
        this.firstDayOfMonth = this.firstDayOfMonth.bind(this);
        this.daysInMonth = this.daysInMonth.bind(this);
    }

    componentDidMount() {
        this.buildCalendar();        
    }

    firstDayOfMonth() {
        let dateObject = this.state.dateObject;
        let firstDay = moment(dateObject)
                     .startOf("month")
                     .format("d"); 

        return firstDay;
    };

    daysInMonth() {
        let dateObject = this.state.dateObject;
        let daysInMonth = moment(dateObject)
                        .daysInMonth();

        return daysInMonth;
    }

    getMonth() {
        let dateObject = this.state.dateObject;
        let month = moment(dateObject)
                    .startOf('month')
                    .format('MMMM');
        return month;
    }

    getYear() {
        let dateObject = this.state.dateObject;
        let year = moment(dateObject)
                    .format('YYYY');
        return year;
    }

    buildCalendar() {
        let firstWeekDay = this.firstDayOfMonth();
        let daysInMonth = this.daysInMonth();
        let calendarFields = [];

        for (let i = 0; i < firstWeekDay; i++) {
            calendarFields.push(
                <td className="field"><p></p></td>
            );
            console.log("blank built");
        }

        for (let i = 1; i <= daysInMonth; i++) {
            calendarFields.push(
                <td className="field"><p>{i}</p></td>
            );
            console.log("day created");
        }

        return this.splitInWeeks(calendarFields);
    }

    splitInWeeks(calendarFields) {
        let row = [];
        let weeks = [];

        calendarFields.forEach((day,i) => {
            i = i+1;
            if ((i !== 0 && i % 7 === 0) || i === calendarFields.length) {
                row.push(day);
                console.log(row)
                weeks.push(<tr className="row-table">{row}</tr>);
                row = [];
            } else {
                row.push(day);
            }
        });

        console.log(weeks);
        return weeks;
    }

    render() {
        return (
            <section className="content-calendar">
                <div className="calendar-header">
                    <p className="month">{this.getMonth()}</p>
                    <p className="year">{this.getYear()}</p>
                </div>
                <table className="table" cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr className="row-header">
                        <td className="field"><p>DOM</p></td>
                        <td className="field"><p>SEG</p></td>
                        <td className="field"><p>TER</p></td>
                        <td className="field"><p>QUA</p></td>
                        <td className="field"><p>QUI</p></td>
                        <td className="field"><p>SEX</p></td>
                        <td className="field"><p>SAB</p></td>
                    </tr>
                    {this.buildCalendar()}
                    {/* <table> */}
                        {/* <tr className="row-table">
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p>1</p></td>
                            <td className="field"><p>1</p></td>
                            <td className="field"><p>1</p></td>
                            <td className="field"><p>1</p></td>
                        </tr>
                        <tr className="row-table">
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                        </tr>
                        <tr className="row-table">
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                        </tr>
                        <tr className="row-table">
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                        </tr>
                        <tr className="row-table">
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                            <td className="field"><p></p></td>
                        </tr> */}
                    {/* </table> */}
                </table>
            </section>
        );
    }
}