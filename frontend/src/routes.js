import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Login from './Components/login/login';
import Home from './Components/home/home';

function Routes() {
    return (
        <Switch>
            <Route path="/login" exact component={Login} />
            <Route path="/" exact component={Home} />
        </Switch>
    )
}
export default Routes;